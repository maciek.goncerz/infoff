# infOFF

This python script provides basic information on Open Font Format file (OTF, TTF).

Based on ISO/IEC 14496-22 (Fourth edition 2019-01)