import io
import struct
import json
from . import reqTables
from . import ttoTables

from .off_types import Type

class OFF_File:
    def __init__(self, filename):
        self._raw = open(filename, 'rb').read()
        self.length = len(self._raw)
        self._bytes_stream = io.BytesIO(self._raw)

        self.header = {}
        self.tables = {}

        self._extractHeader()
        self._extractTableHeaders()
        self._extractTables()
    
    def __repr__(self):
        return 'Header:\n{}\nTable:\n{}\n'.format(self.header, self.tables)

    def save(self, filename):
        with open(filename, 'w') as fs:
            json.dump({
                'tables': self.tables
            }, fs)

    def _extractTables(self):
        for key in self.tables:
            # hmtx has 2 more arguments and should be parsed at the end
            if key == 'hmtx':
                continue

            # handle required tables
            if key in reqTables.byTag:
                self.tables[key]['data'] = reqTables.byTag[key](
                    self._bytes_stream, 
                    self.tables[key]
                )
                continue

            # handle TTO tables
            if key in ttoTables.byTag:
                # these has arguments dependent on some required tables
                if key == 'glyf' or key == 'loca':
                    continue

                self.tables[key]['data'] = ttoTables.byTag[key](
                    self._bytes_stream, 
                    self.tables[key]
                )
                continue
                
            # TODO: handle CFF tables
            # TODO: handle SVG tables
            # TODO: handle bitmap tables
            # TODO: handle optional tables

        self.tables['hmtx']['data'] = reqTables.byTag['hmtx'](
                self._bytes_stream, 
                self.tables['hmtx'],
                self.tables['hhea']['data']['numberOfHMetrics'],
                self.tables['maxp']['data']['numGlyphs']
            )

        if 'loca' in self.tables:
            self.tables['loca']['data'] = ttoTables.byTag['loca'](
                    self._bytes_stream, 
                    self.tables['loca'],
                    self.tables['maxp']['data']['numGlyphs'],
                    self.tables['head']['data']['indexToLocFormat']
                )

        if 'glyf' in self.tables:
            self.tables['glyf']['data'] = ttoTables.byTag['glyf'](
                    self._bytes_stream, 
                    self.tables['glyf'],
                    self.tables['maxp']['data']['numGlyphs'],
                    self.tables['loca']['data']
                )

    def _extractHeader(self):
        b = self._bytes_stream

        self.header['sfntVersion'] = b.read(4)
        self.header['numTables'] = Type.uint16(b)
        self.header['searchRange'] = Type.uint16(b)
        self.header['entrySelector'] = Type.uint16(b)
        self.header['rangeShift'] = Type.uint16(b)

    def _extractTable(self):
        b = self._bytes_stream
        
        table = {}
        table['tableTag'] = Type.Tag(b)
        table['checksum'] = Type.uint32(b)
        table['Offset'] = Type.Offset32(b)
        table['Length'] = Type.uint32(b)

        assert self._validateChecksum(table)

        self.tables[table['tableTag']] = table

    def _extractTableHeaders(self):
        for _ in range(self.header['numTables']):
            self._extractTable()

    def _validateChecksum(self, table):
        if self.length < table['Offset'] + table['Length']:
            return False

        # for now due to checkcSumAddjustment complications
        if table['tableTag'] == 'head':
            return True

        pointer = self._bytes_stream.tell()
        self._bytes_stream.seek(table['Offset'])

        sum = 0
        length = int((table['Length'] + 3) / 4)
        for index in range(length):
            sum += Type.uint32(self._bytes_stream)

        sum = sum & 0xFFFFFFFF

        self._bytes_stream.seek(pointer)
        return sum == table['checksum']