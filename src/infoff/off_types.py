import struct

class Type:
    def int8(bytes):
        return struct.unpack('>b', bytes.read(1))[0]
    def uint8(bytes):
        return struct.unpack('>B', bytes.read(1))[0]
    def int16(bytes):
        return struct.unpack('>h', bytes.read(2))[0]
    def uint16(bytes):
        return struct.unpack('>H', bytes.read(2))[0]
    def uint24(bytes):
        ba = bytearray(bytes.read(3))
        ba[0:0] = 0
        return struct.unpack('>I', bytes(ba))[0]
    def int32(bytes):
        return struct.unpack('>i', bytes.read(4))[0]
    def uint32(bytes):
        return struct.unpack('>I', bytes.read(4))[0]
    def LONGDATETIME(bytes):
        return struct.unpack('>q', bytes.read(8))[0]
    def Tag(bytes):
        return bytes.read(4).decode('utf-8')

    Fixed = int32
    F2DOT14 = int16
    FWORD = int16
    UFWORD = uint16
    Offset16 = uint16
    Offset32 = uint32