from ..reqTables import cmap
from ..reqTables import head
from ..reqTables import hhea
from ..reqTables import hmtx
from ..reqTables import maxp
from ..reqTables import name
from ..reqTables import os2
from ..reqTables import post

"""
    :param stream: io.BytesIO stream of OFF file
    :param table: dict containing concerned table information

    :return: dictionary describing concerned table tag
    :rtype: dict
"""

cmap = cmap.generate
head = head.generate
hhea = hhea.generate
hmtx = hmtx.generate
maxp = maxp.generate
name = name.generate
os2 = os2.generate
post = post.generate

byTag = {
    'cmap' : cmap,
    'head' : head,
    'hhea' : hhea,
    'hmtx' : hmtx,
    'maxp' : maxp,
    'name' : name,
    'OS/2' : os2,
    'post' : post
}