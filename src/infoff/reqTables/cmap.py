from ..off_types import Type

description = """This table defines the mapping of character codes to a default glyph index. Different subtables may be defined
that each contain mappings for different character encoding schemes. The table header indicates the
character encodings for which subtables are present.
Regardless of the encoding scheme, character codes that do not correspond to any glyph in the font should
be mapped to glyph index 0. The glyph at this location must be a special glyph representing a missing
character, commonly known as .notdef.
Each subtable is in one of seven possible formats and begins with a format code indicating the format used.
The first four formats — formats 0, 2, 4 and 6 — were originally defined prior to Unicode 2.0. These formats
allow for 8-bit single-byte, 8-bit multi-byte, and 16-bit encodings. With the introduction of supplementary
planes in Unicode 2.0, the Unicode addressable code space extends beyond 16 bits. To accommodate this,
three additional formats were added — formats 8, 10 and 12 — that allow for 32-bit encoding schemes.
Other enhancements in Unicode led to the addition of other subtable formats. Subtable format 13 allows for an
efficient mapping of many characters to a single glyph; this is useful for “last-resort” fonts that provide fallback
rendering for all possible Unicode characters with a distinct fallback glyph for different Unicode ranges.
Subtable format 14 provides a unified mechanism for supporting Unicode variation sequences."""

def generate(stream, table):
    pointer = stream.tell()
    stream.seek(table['Offset'])
    header = {}

    header['Version'] = Type.uint16(stream)
    header['numTables'] = Type.uint16(stream)
    header['encodingRecords'] = []
    for _ in range(header['numTables']):
        d = {
            'platformID': Type.uint16(stream),
            'encodingID': Type.uint16(stream),
            'offset': Type.Offset32(stream)
        }
        header['encodingRecords'].append(d)

        pointer_prim = stream.tell()
        stream.seek(table['Offset'] + d['offset'])

        d['format'] = Type.uint16(stream)

        if d['format'] == 0:
            d['length'] = Type.uint16(stream)
            d['language'] = Type.uint16(stream)
            d['glyphIdArray'] = []
            for _ in range(256):
                d['glyphIdArray'].append(Type.uint8(stream))

        elif d['format'] == 2:
            d['length'] = Type.uint16(stream)
            d['language'] = Type.uint16(stream)
            print('format 2 needed')
            pass

        elif d['format'] == 4:
            d['length'] = Type.uint16(stream)
            d['language'] = Type.uint16(stream)
            d['segCountX2'] = Type.uint16(stream)
            d['searchRange'] = Type.uint16(stream)
            d['entrySelector'] = Type.uint16(stream)
            d['rangeShift'] = Type.uint16(stream)
            d['endCode'] = []
            for _ in range(int(d['segCountX2'] / 2)):
                d['endCode'].append(Type.uint16(stream))
            d['reservedPad'] = Type.uint16(stream)
            d['startCode'] = []
            for _ in range(int(d['segCountX2'] / 2)):
                d['startCode'].append(Type.uint16(stream))
            d['idDelta'] = []
            for _ in range(int(d['segCountX2'] / 2)):
                d['idDelta'].append(Type.uint16(stream))
            d['idRangeOffset'] = []
            for _ in range(int(d['segCountX2'] / 2)):
                d['idRangeOffset'].append(Type.uint16(stream))
            d['glyphIdArray'] = []
            glyph_length = d['length'] - (16 + int(d['segCountX2'] / 2) * 8)
            for _ in range(glyph_length):
                d['glyphIdArray'].append(Type.uint16(stream))
            
        elif d['format'] == 6:
            d['length'] = Type.uint16(stream)
            d['language'] = Type.uint16(stream)
            d['firstCode'] = Type.uint16(stream)
            d['entryCount'] = Type.uint16(stream)
            d['glyphIdArray'] = []
            for _ in range(d['entryCount']):
                d['glyphIdArray'].append(Type.uint16(stream))
                
        elif d['format'] == 8:
            d['reserved'] = Type.uint16(stream)
            d['length'] = Type.uint32(stream)
            d['language'] = Type.uint32(stream)
            d['is32'] = []
            for _ in range(8192):
                d['is32'].append(Type.uint8(stream))
            d['numGroups'] = Type.uint32(stream)
            d['groups'] = []
            for _ in range(d['numGroups']):
                d['groups'].append({
                    'startCharCode': Type.uint32(stream),
                    'endCharCode': Type.uint32(stream),
                    'startGlyphID': Type.uint32(stream)
                })
                
        elif d['format'] == 10:
            d['reserved'] = Type.uint16(stream)
            d['length'] = Type.uint32(stream)
            d['language'] = Type.uint32(stream)
            d['startCharCode'] = Type.uint32(stream)
            d['numChars'] = Type.uint32(stream)
            d['glyphs'] = []
            for _ in range(d['numChars']):
                d['glyphs'].append(Type.uint16(stream))

        elif d['format'] == 12:
            d['reserved'] = Type.uint16(stream)
            d['length'] = Type.uint32(stream)
            d['language'] = Type.uint32(stream)
            d['numGroups'] = Type.uint32(stream)
            d['groups'] = []
            for _ in range(d['numGroups']):
                d['groups'].append({
                    'startCharCode': Type.uint32(stream),
                    'endCharCode': Type.uint32(stream),
                    'startGlyphID': Type.uint32(stream)
                })
                
        elif d['format'] == 13:
            d['reserved'] = Type.uint16(stream)
            d['length'] = Type.uint32(stream)
            d['language'] = Type.uint32(stream)
            d['numGroups'] = Type.uint32(stream)
            d['groups'] = []
            for _ in range(d['numGroups']):
                d['groups'].append({
                    'startCharCode': Type.uint32(stream),
                    'endCharCode': Type.uint32(stream),
                    'glyphID': Type.uint32(stream)
                })
                
        elif d['format'] == 14:
            d['length'] = Type.uint32(stream)
            d['numVarSelectorRecords'] = Type.uint32(stream)
            d['varSelector'] = []
            for _ in range(d['numVarSelectorRecords']):
                d['varSelector'].append({
                    'varSelector': Type.uint24(stream),
                    'defaultUVSOffset': Type.Offset32(stream),
                    'nonDefaultUVSOffset': Type.Offset32(stream)
                })

            # load varSelectors
            for ind in range(d['numVarSelectorRecords']):
                dUVS = d['varSelector'][ind]['defaultUVSOffset'] + pointer_prim
                ndUVS = d['varSelector'][ind]['nonDefaultUVSOffset'] + pointer_prim

                if dUVS > pointer_prim: # non-zero offset
                    stream.seek(dUVS)
                    d['varSelector'][ind]['numUnicodeValueRanges'] = Type.uint32(stream)
                    d['varSelector'][ind]['ranges'] = []
                    for _ in range(d['varSelector'][ind]['numUnicodeValueRanges']):
                        d['varSelector'][ind]['ranges'].append({
                            'startUnicodeValue': Type.uint24(stream),
                            'additionalCount': Type.uint8(stream)
                        })
                        
                if ndUVS > pointer_prim: # non-zero offset
                    stream.seek(ndUVS)
                    d['varSelector'][ind]['numUVSMappings'] = Type.uint32(stream)
                    d['varSelector'][ind]['uvsMappings'] = []
                    for _ in range(d['varSelector'][ind]['numUVSMappings']):
                        d['varSelector'][ind]['uvsMappings'].append({
                            'unicodeValue': Type.uint24(stream),
                            'glyphID': Type.uint16(stream)
                        })

        stream.seek(pointer_prim)

    stream.seek(pointer)
    return header


