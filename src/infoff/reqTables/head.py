from ..off_types import Type

description = """This table gives global information about the font. The bounding box values should be computed using only
glyphs that have contours. Glyphs with no contours should be ignored for the purposes of these calculations."""

def generate(stream, table):
    pointer = stream.tell()
    stream.seek(table['Offset'])
    header = {}

    header['majorVersion'] = Type.uint16(stream)
    header['minorVersion'] = Type.uint16(stream)
    header['fontRevision'] = Type.Fixed(stream)
    header['checkSumAdjustment_pos'] = stream.tell() # to calculate head checksum at the end
    header['checkSumAdjustment'] = Type.uint32(stream)
    header['magicNumber'] = Type.uint32(stream)
    header['flags'] = Type.uint16(stream)
    header['unitsPerEm'] = Type.uint16(stream)
    header['created'] = Type.LONGDATETIME(stream)
    header['modified'] = Type.LONGDATETIME(stream)
    header['xMin'] = Type.int16(stream)
    header['yMin'] = Type.int16(stream)
    header['xMax'] = Type.int16(stream)
    header['yMax'] = Type.int16(stream)
    header['macStyle'] = Type.uint16(stream)
    header['lowestRecPPEM'] = Type.uint16(stream)
    header['fontDirectionHint'] = Type.int16(stream)
    header['indexToLocFormat'] = Type.int16(stream)
    header['glyphDataFormat'] = Type.int16(stream)

    stream.seek(pointer)
    return header
