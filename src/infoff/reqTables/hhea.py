from ..off_types import Type

description = """This table contains information for horizontal layout. The values in the minRightSidebearing,
minLeftSideBearing and xMaxExtent should be computed using only glyphs that have contours. Glyphs with
no contours should be ignored for the purposes of these calculations. All reserved areas shall be set to 0."""

def generate(stream, table):
    pointer = stream.tell()
    stream.seek(table['Offset'])
    header = {}

    header['majorVersion'] = Type.uint16(stream)
    header['minorVersion'] = Type.uint16(stream)
    header['ascender'] = Type.FWORD(stream)
    header['descender'] = Type.FWORD(stream)
    header['lineGap'] = Type.FWORD(stream)
    header['advanceWidthMax'] = Type.UFWORD(stream)
    header['minLeftSideBearing'] = Type.FWORD(stream)
    header['minRightSideBearing'] = Type.FWORD(stream)
    header['xMaxExtent'] = Type.FWORD(stream)
    header['caretSlopeRise'] = Type.int16(stream)
    header['caretSlopeRun'] = Type.int16(stream)
    header['caretOffset'] = Type.int16(stream)
    header['(reserver)'] = Type.int16(stream)
    header['(reserver)'] = Type.int16(stream)
    header['(reserver)'] = Type.int16(stream)
    header['(reserver)'] = Type.int16(stream)
    header['metricDataFormat'] = Type.int16(stream)
    header['numberOfHMetrics'] = Type.uint16(stream)

    stream.seek(pointer)
    return header
