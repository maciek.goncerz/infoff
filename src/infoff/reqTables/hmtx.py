from ..off_types import Type

description = """Glyph metrics used for horizontal text layout include glyph advance widths, side bearings and X-direction min
and max values (xMin, xMax). These are derived using a combination of the glyph outline data ('glyf', 'CFF ' or
'CFF2') and the horizontal metrics table. The horizontal metrics ('hmtx') table provides glyph advance widths
and left side bearings.
In a font with TrueType outline data, the 'glyf' table provides xMin and xMax values, but not advance widths or
side bearings. The advance width is always obtained from the 'hmtx' table. In some fonts, depending on the
state of flags in the 'head' table, the left side bearings may be the same as the xMin values in the 'glyf' table,
though this is not true for all fonts. (See the description of bit 1 of the flags field in the 'head' table.) For this
reason, left side bearings are provided in the 'hmtx' table. The right side bearing is always derived using
advance width and left side bearing values from the 'hmtx' table, plus bounding-box information in the glyph
description — see below for more details.
In a variable font with TrueType outline data, the left side bearing value in the 'hmtx' table must always be
equal to xMin (bit 1 of the 'head' flags field must be set). Hence, these values can also be derived directly from
the 'glyf' table. Note that these values apply only to the default instance of the variable font: non-default
instances may have different side bearing values. These can be derived from interpolated “phantom point”
coordinates using the 'gvar' table (see below for additional details), or by applying variation data in the 'HVAR'
table to default-instance values from the 'glyf' or 'hmtx' table.
In a font with CFF version 1 outline data, the 'CFF ' table does include advance widths. These values are used
by PostScript processors, but are not used in OFF layout. In an OFF context, the 'hmtx' table is required and
must be used for advance widths. Note that fonts in a Font Collection file that share a 'CFF ' table may specify
different advance widths in font-specific 'hmtx' tables for a particular glyph index. Also note that the 'CFF2'
table does not include advance widths. In addition, for either 'CFF ' or 'CFF2' data, there are no explicit xMin
and xMax values; side bearings are implicitly contained within the CharString data, and can be obtained from
the the CFF / CFF2 rasterizer. Some layout engines may use left side bearing values in the 'hmtx' table,
however; hence, font production tools should ensure that the lsb values in the 'hmtx' table match the implicit
xMin values reflected in the CharString data. In a variable font with CFF2 outline data, left side bearing and
advance width values for non-default instances should be obtained by combining information from the 'hmtx'
and 'HVAR' tables.
The table uses a longHorMetric record to give the advance width and left side bearing of a glyph. Records are
indexed by glyph ID. As an optimization, the number of records can be less than the number of glyphs, in
which case the advance width value of the last record applies to all remaining glyph IDs. This can be useful in
monospaced fonts, or in fonts that have a large number of glyphs with the same advance width (provided the
glyphs are ordered appropriately). The number of longHorMetric records is determined by the
numberOfHMetrics field in the 'hhea' table.
If the longHorMetric array is less than the total number of glyphs, then that array is followed by an array for the
left side bearing values of the remaining glyphs. The number of elements in the left side bearing will be
derived from numberOfHMetrics plus the numGlyphs field in the 'maxp' table."""

# numberOfHMetrics - hhea table
# numGlyphs - maxp table
def generate(stream, table, numberOfHMetrics, numGlyphs):
    pointer = stream.tell()
    stream.seek(table['Offset'])
    header = {}

    header['hMetrics'] = []
    for _ in range(numberOfHMetrics):
        header['hMetrics'].append({
            'advanceWidth': Type.uint16(stream),
            'lsb': Type.int16(stream)
        })

    header['leftSideBearing'] = []
    for _ in range(max(numGlyphs - numberOfHMetrics, 0)):
        header['leftSideBearing'].append(
            Type.int16(stream)
        )

    stream.seek(pointer)
    return header


