from ..off_types import Type

description = """This table establishes the memory requirements for this font. Fonts with CFF data must use Version 0.5 of this
table, specifying only the numGlyphs field. Fonts with TrueType outlines must use Version 1.0 of this table,
where all data is required."""

def generate(stream, table):
    pointer = stream.tell()
    stream.seek(table['Offset'])
    header = {}

    header['version'] = Type.Fixed(stream)
    header['numGlyphs'] = Type.uint16(stream)

    if header['version'] == 0x00010000:
        header['maxPoints'] = Type.uint16(stream)
        header['maxContours'] = Type.uint16(stream)
        header['maxCompositePoints'] = Type.uint16(stream)
        header['maxCompositeContours'] = Type.uint16(stream)
        header['maxZones'] = Type.uint16(stream)
        header['maxTwilightPoints'] = Type.uint16(stream)
        header['maxStorage'] = Type.uint16(stream)
        header['maxFunctionDefs'] = Type.uint16(stream)
        header['maxInstructionDefs'] = Type.uint16(stream)
        header['maxStackElements'] = Type.uint16(stream)
        header['maxSizeOfInstructions'] = Type.uint16(stream)
        header['maxComponentElements'] = Type.uint16(stream)
        header['maxComponentDepth'] = Type.uint16(stream)

    stream.seek(pointer)
    return header


