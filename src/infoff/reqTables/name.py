from ..off_types import Type

description = """"""

def generate(stream, table):
    pointer = stream.tell()
    stream.seek(table['Offset'])
    header = {}

    header['format'] = Type.uint16(stream)
    header['count'] = Type.uint16(stream)
    header['stringOffset'] = Type.Offset16(stream) # from start of table
    header['nameRecord'] = []
    for _ in range(header['count']):
        header['nameRecord'].append({
            'platformID': Type.uint16(stream),
            'encodingID': Type.uint16(stream),
            'languageID': Type.uint16(stream),
            'nameID': Type.uint16(stream),
            'length': Type.uint16(stream),
            'Offset': Type.Offset16(stream)
        })
    # TODO: load string names for nameRecord

    if header['format'] == 1:
        header['langTagCount'] = Type.uint16(stream)
        header['langTagRecord'] = []
        for _ in range(header['langTagCount']):
            header['langTagRecord'].append({
                'length': Type.uint16(stream),
                'offset': Type.uint16(stream)
            })
    # TODO: load string tag for langTagRecord

    stream.seek(pointer)
    return header


