from ..off_types import Type

description = """The OS/2 table consists of a set of metrics and other data that are required in OFF fonts."""

def generate(stream, table):
    pointer = stream.tell()
    stream.seek(table['Offset'])
    header = {}

    header['Version'] = Type.uint16(stream)
    header['xAvgCharWidth'] = Type.int16(stream)
    header['usWeightClass'] = Type.uint16(stream)
    header['usWidthClass'] = Type.uint16(stream)
    header['fsType'] = Type.uint16(stream)
    header['ySubscriptXSize'] = Type.int16(stream)
    header['ySubscriptYSize'] = Type.int16(stream)
    header['ySubscriptXOffset'] = Type.int16(stream)
    header['ySubscriptYOffset'] = Type.int16(stream)
    header['ySuperscriptXSize'] = Type.int16(stream)
    header['ySuperscriptYSize'] = Type.int16(stream)
    header['ySuperscriptXOffset'] = Type.int16(stream)
    header['ySuperscriptYOffset'] = Type.int16(stream)
    header['yStrikeoutSize'] = Type.int16(stream)
    header['yStrikeoutPosition'] = Type.int16(stream)
    header['sFamilyClass'] = Type.int16(stream)
    header['Panose'] = {
        'bFamilyType': Type.uint8(stream),
        'bSerifStyle': Type.uint8(stream),
        'bWeight': Type.uint8(stream),
        'bProportion': Type.uint8(stream),
        'bContrast': Type.uint8(stream),
        'bStrokeVariation': Type.uint8(stream),
        'bArmStyle': Type.uint8(stream),
        'bLetterform': Type.uint8(stream),
        'bMidline': Type.uint8(stream),
        'bXHeight': Type.uint8(stream)
    }
    header['ulUnicodeRange1'] = Type.uint32(stream)
    if header['Version'] >= 0x0001:
        header['ulUnicodeRange2'] = Type.uint32(stream)
        header['ulUnicodeRange3'] = Type.uint32(stream)
        header['ulUnicodeRange4'] = Type.uint32(stream)
    header['achVendID'] = Type.Tag(stream)
    header['fsSelection'] = Type.uint16(stream)
    header['usFirstCharIndex'] = Type.uint16(stream)
    header['usLastCharIndex'] = Type.uint16(stream)
    header['sTypoAscender'] = Type.int16(stream)
    header['sTypoDescender'] = Type.int16(stream)
    header['sTypoLineGap'] = Type.int16(stream)
    header['usWinAscent'] = Type.uint16(stream)
    header['usWinDescent'] = Type.uint16(stream)
    if header['Version'] >= 0x0001:
        header['ulCodePageRange1'] = Type.uint32(stream)
        header['ulCodePageRange2'] = Type.uint32(stream)
    if header['Version'] >= 0x0002:
        header['sxHeight'] = Type.int16(stream)
        header['sCapHeight'] = Type.int16(stream)
        header['usDefaultChar'] = Type.uint16(stream)
        header['usBreakChar'] = Type.uint16(stream)
        header['usMaxContext'] = Type.uint16(stream)
    if header['Version'] >= 0x0005:
        header['usLowerOpticalPointSize'] = Type.uint16(stream)
        header['usUpperOpticalPointSize'] = Type.uint16(stream)

    # append MVAR table
    header['MVAR'] = {
        'hasc': header['sTypoAscender'],
        'hdsc': header['sTypoDescender'],
        'hlgp': header['sTypoLineGap'],
        'hcla': header['usWinAscent'],
        'hcld': header['usWinDescent'],
        'stro': header['yStrikeoutPosition'],
        'strs': header['yStrikeoutSize'],
        'sbxo': header['ySubscriptXOffset'],
        'sbxs': header['ySubscriptXSize'],
        'sbyo': header['ySubscriptYOffset'],
        'sbys': header['ySubscriptYSize'],
        'spxo': header['ySuperscriptXOffset'],
        'spxs': header['ySuperscriptXSize'],
        'spyo': header['ySuperscriptYOffset'],
        'spys': header['ySuperscriptYSize']
    }

    if header['Version'] >= 0x0002:
        header['MVAR']['cpht'] = header['sCapHeight']
        header['MVAR']['cpht'] = header['sxHeight']

    stream.seek(pointer)
    return header


