from ..off_types import Type

description = """This table contains additional information needed to use TrueType or OFF fonts on PostScript printers. This
includes data for the FontInfo dictionary entry and the PostScript names of all the glyphs. For more
information about PostScript names, see the Adobe document Unicode and Glyph Names in Reference [3].
Table Versions 1.0, 2.0, and 2.5 refer to TrueType fonts and OFF fonts with TrueType data. OFF fonts with
TrueType data may also use Version 3.0. OFF fonts with CFF data use Version 3.0 only.
The last four entries in the table are present because PostScript drivers can do better memory management if
the virtual memory (VM) requirements of a downloadable OFF font are known before the font is downloaded.
This information should be supplied if known. If it is not known, set the value to zero. The driver will still work
but will be less efficient.
Maximum memory usage is minimum memory usage plus maximum runtime memory use. Maximum runtime
memory use depends on the maximum band size of any bitmap potentially rasterized by the font scaler.
Runtime memory usage could be calculated by rendering characters at different point sizes and comparing
memory use.
If the table version is 1.0 or 3.0, the table ends here. The additional entries for versions 2.0 and 2.5 are shown
below. Version 4.0 is reserved to the specification published in Reference [7].
Version 1.0
This version is used in order to supply PostScript glyph names when the font file contains exactly the 258
glyphs in the standard Macintosh TrueType font file (see 'post' Format 1 in Reference [2] for a list of the 258
Macintosh glyph names), and the font does not otherwise supply glyph names. As a result, the glyph names
are taken from the system with no storage required by the font.
Version 2.0
This is the version required in order to supply PostScript glyph names for fonts which do not supply them
elsewhere. A version 2.0 'post' table can be used in fonts with TrueType or CFF version 2 outlines.
This font file contains glyphs not in the standard Macintosh set or the ordering of the glyphs in the font file
differs from the standard Macintosh set. The glyph name array maps the glyphs in this font to name index. If
the name index is between 0 and 257, treat the name index as a glyph index in the Macintosh standard order.
If the name index is between 258 and 65535, then subtract 258 and use that to index into the list of Pascal
strings at the end of the table. Thus a given font may map some of its glyphs to the standard glyph names,
and some to its own names.
If you do not want to associate a PostScript name with a particular glyph, use index number 0 which points the
name .notdef.
Version 2.5
This version of the 'post' table has been deprecated.
Version 3.0
This version makes it possible to create a font that is not burdened with a large 'post' table set of glyph names.
A version 3.0 'post' table can be used by OFF fonts with TrueType or CFF (version 1 or 2) data.
This version specifies that no PostScript name information is provided for the glyphs in this font file. The
printing behavior of this version on PostScript printers is unspecified, except that it should not result in a fatal
or unrecoverable error. Some drivers may print nothing, other drivers may attempt to print using a default
naming scheme.
Windows makes use of the italic angle value in the 'post' table but does not actually require any glyph names
to be stored as Pascal strings."""

def generate(stream, table):
    pointer = stream.tell()
    stream.seek(table['Offset'])
    header = {}

    header['version'] = Type.Fixed(stream)
    header['italicAngle'] = Type.Fixed(stream)
    header['underlinePosition'] = Type.FWORD(stream)
    header['underlineThickness'] = Type.FWORD(stream)
    header['isFixedPitch'] = Type.uint32(stream)
    header['minMemType42'] = Type.uint32(stream)
    header['maxMemType42'] = Type.uint32(stream)
    header['minMemType1'] = Type.uint32(stream)
    header['maxMemType1'] = Type.uint32(stream)

    if header['version'] == 0x00020000:
        header['numGlyphs'] = Type.uint16(stream)
        header['glyphNameIndex'] = []
        for _ in range(header['numGlyphs']):
            header['glyphNameIndex'].append(
                Type.uint16(stream)
            )
        # next part (page 78) needs numberNewGlyphs variable that is not provied

    stream.seek(pointer)
    return header


