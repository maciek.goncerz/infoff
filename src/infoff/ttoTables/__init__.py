from ..ttoTables import cvt
from ..ttoTables import fpgm
from ..ttoTables import glyf
from ..ttoTables import loca
from ..ttoTables import prep
from ..ttoTables import gasp

"""
    :param stream: io.BytesIO stream of OFF file
    :param table: dict containing concerned table information

    :return: dictionary describing concerned table tag
    :rtype: dict
"""

cvt = cvt.generate
fpgm = fpgm.generate
glyf = glyf.generate
loca = loca.generate
prep = prep.generate
gasp = gasp.generate

byTag = {
    'cvt ' : cvt,
    'fpgm' : fpgm,
    'glyf' : glyf,
    'loca' : loca,
    'prep' : prep,
    'gasp' : gasp
}