from ..off_types import Type

description = """This table contains a list of values that can be referenced by instructions. They can be used, among other
things, to control characteristics for different glyphs. The length of the table must be an integral number of
FWORD units."""

def generate(stream, table):
    pointer = stream.tell()
    stream.seek(table['Offset'])
    header = []
    
    for _ in range(int(table['Length'] / 2)):
        header.append(Type.FWORD(stream))

    stream.seek(pointer)
    return header
