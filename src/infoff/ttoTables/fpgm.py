from ..off_types import Type

description = """This table is similar to the CV Program, except that it is only run once, when the font is first used. It is used
only for FDEFs and IDEFs. Thus the CV Program need not contain function definitions. However, the CV
Program may redefine existing FDEFs or IDEFs."""

def generate(stream, table):
    pointer = stream.tell()
    stream.seek(table['Offset'])
    header = []
    
    for _ in range(table['Length']):
        header.append(Type.uint8(stream))

    stream.seek(pointer)
    return header
