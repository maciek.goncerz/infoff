from ..off_types import Type

description = """This table contains information which describes the preferred rasterization techniques for the typeface when it
is rendered on grayscale-capable devices. This table also has some use for monochrome devices, which may
use the table to turn off hinting at very large or small sizes, to improve performance.
At very small sizes, the best appearance on grayscale devices can usually be achieved by rendering the
glyphs in grayscale without using hints. At intermediate sizes, hinting and monochrome rendering will usually
produce the best appearance. At large sizes, the combination of hinting and grayscale rendering will typically
produce the best appearance.
If the 'gasp' table is not present in a typeface, the rasterizer may apply default rules to decide how to render
the glyphs on grayscale devices.
The 'gasp' table consists of a header followed by groupings of 'gasp' records."""

def generate(stream, table):
    pointer = stream.tell()
    stream.seek(table['Offset'])
    header = {}
    
    header['version'] = Type.uint16(stream)
    header['numRanges'] = Type.uint16(stream)

    header['gaspRanges'] = []
    for _ in range(header['numRanges']):
        header['gaspRanges'].append({
            'rangeMaxPPEM': Type.uint16(stream),
            'rangeGaspBehavior': Type.uint16(stream)
        })

    stream.seek(pointer)
    return header
