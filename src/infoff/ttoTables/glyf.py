from ..off_types import Type

description = """This table contains information that describes the glyphs in the font in the TrueType outline format. Information
regarding the rasterizer (scaler) refers to the TrueType rasterizer.
Table organization
The 'glyf' table is comprised of a list of glyph data blocks, each of which provides the description for a single
glyph. Glyphs are referenced by identifiers (glyph IDs), which are sequential integers beginning at zero. The
total number of glyphs is specified by the numGlyphs field in the 'maxp' table. The 'glyf' table does not include
any overall table header or records providing offsets to glyph data blocks. Rather, the 'loca' table provides an
array of offsets, indexed by glyph IDs, which provide the location of each glyph data block within the 'glyf' table.
Note that the 'glyf' table must always be used in conjunction with the 'loca' and 'maxp' tables. The size of each
glyph data block is inferred from the difference between two consecutive offsets in the 'loca' table (with one
extra offset provided to give the size of the last glyph data block). As a result of the 'loca' format, glyph data
blocks within the 'glyf' table must be in glyph ID order."""

# numGlyphs - in maxp table
# loca - whole loca table
def generate(stream, table, numGlyphs, loca):
    pointer = stream.tell()
    stream.seek(table['Offset'])
    header = []

    for index in range(0, numGlyphs):
        length = loca[index + 1] - loca[index]

        if length < 10:
            header.append({})
            continue

        stream.seek(table['Offset'] + loca[index])

        glyph = {
            'numberOfContours': Type.int16(stream),
            'xMin': Type.int16(stream),
            'yMin': Type.int16(stream),
            'xMax': Type.int16(stream),
            'yMax': Type.int16(stream)
        }
        
        # TODO: repair errors in reading contours
        header.append(glyph)
        continue

        # for non-composite glyph
        if glyph['numberOfContours'] >= 0:
            glyph['endPtsOfContours'] = []
            for _ in range(glyph['numberOfContours']):
                glyph['endPtsOfContours'].append(
                    Type.uint16(stream)
                )
            glyph['instructionLength'] = Type.uint16(stream)
            glyph['instructions'] = []
            for _ in range(glyph['instructionLength']):
                glyph['instructions'].append(
                    Type.uint8(stream)
                )

            glyph['flags'] = []
            glyph['xCoordinates'] = []
            glyph['yCoordinates'] = []
            while stream.tell() < loca[index + 1]:
                flag = Type.uint8(stream)
                glyph['flags'].append(flag)

                # x_coordinate
                if flag & 0x02:
                    glyph['xCoordinates'].append(Type.uint8(stream))
                else:
                    glyph['xCoordinates'].append(Type.int16(stream))
                    
                # y_coordinate
                if flag & 0x04:
                    glyph['yCoordinates'].append(Type.uint8(stream))
                else:
                    glyph['yCoordinates'].append(Type.int16(stream))

        # for composite glyph
        else:
            glyph['Flags'] = []
            glyph['glyphIndex'] = []
            glyph['Argument1'] = []
            glyph['Argument2'] = []

            flag = 0x0020
            while flag & 0x0020:
                flag = Type.uint16(stream)
                glyph['Flags'].append(flag)
                glyph['glyphIndex'].append(Type.uint16(stream))

                if flag & 0x0001: # ARG_1_AND_2_ARE_WORDS
                    glyph['Argument1'].append(Type.int16(stream))
                    glyph['Argument2'].append(Type.int16(stream))
                else:
                    glyph['Argument1'].append(Type.uint8(stream))
                    glyph['Argument2'].append(Type.uint8(stream))

                if flag & 0x0008: # WE_HAVE_A_SCALE
                    glyph['scale'] = Type.F2DOT14(stream)
                
                elif flag & 0x0040: # WE_HAVE_AN_X_AND_Y_SCALE
                    glyph['xscale'] = Type.F2DOT14(stream)
                    glyph['yscale'] = Type.F2DOT14(stream)
                
                elif flag & 0x0080: # WE_HAVE_A_TWO_BY_TWO
                    glyph['xscale'] = Type.F2DOT14(stream)
                    glyph['scale01'] = Type.F2DOT14(stream)
                    glyph['scale10'] = Type.F2DOT14(stream)
                    glyph['yscale'] = Type.F2DOT14(stream)

            if flag & 0x0100: # WE_HAVE_INSTRUCTIONS
                glyph['numInstr'] = Type.uint16(stream)
                glyph['instr'] = []
                for _ in range(glyph['numInstr']):
                    glyph['instr'].append(Type.uint8(stream))

        header.append(glyph)

    stream.seek(pointer)
    return header
