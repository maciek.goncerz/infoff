from ..off_types import Type

description = """The loca table stores the offsets to the locations of the glyphs in the font, relative to the beginning of the glyf
table. In order to compute the length of the last glyph element, there is an extra entry after the last valid index.
By definition, index zero points to the "missing character", which is the character that appears if a character is
not found in the font. The missing character is commonly represented by a blank box or a space. If the font
does not contain an outline for the missing character, then the first and second offsets should have the same
value. This also applies to any other character without an outline, such as the space character. If a glyph has
no outlines, the offset loca[n] = loca[n+1]. In the particular case of the last glyph(s), loca[n] will be equal the
length of the glyph data ('glyf') table. The offsets shall be in ascending order with loca[n] <= loca[n+1].
Most routines will look at the 'maxp' table to determine the number of glyphs in the font, but the value in the
'loca' table should agree.
There are two versions of this table, the short and the long. The version is specified in the indexToLocFormat
entry in the 'head' table."""

# numGlyphs - in maxp table
# indexToLocFormat - in head table
def generate(stream, table, numGlyphs, indexToLocFormat):
    pointer = stream.tell()
    stream.seek(table['Offset'])
    header = []

    if indexToLocFormat == 0:
        for _ in range(numGlyphs + 1):
            header.append(Type.Offset16(stream))
    elif indexToLocFormat == 1:
        for _ in range(numGlyphs + 1):
            header.append(Type.Offset32(stream))

    stream.seek(pointer)
    return header
