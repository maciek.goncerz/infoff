import sys
import infoff

if __name__ == '__main__':
    if len(sys.argv) != 2:
        exit()

    off = infoff.OFF_File(sys.argv[1])
    off.save(sys.argv[1].replace('.otf', '.json'))